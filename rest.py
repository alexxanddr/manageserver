#!flask/bin/python
from flask import Flask,jsonify
from flask_httpauth import HTTPBasicAuth
from Rasp import Rasp
from Netw import Netw
import os,signal,psutil,json

app = Flask(__name__)
auth = HTTPBasicAuth()

users = {
    "alexxanddr": "iPod1993#"
}

rasp=Rasp()
netw=Netw()

@auth.get_password
def get_pw(username):
    if username in users:
        return users.get(username)
    return None

@app.route('/')
#@auth.login_required
def index():
	return jsonify({'User ':auth.username()})

@app.route('/api/getCpuTemp')
#@auth.login_required
def getTemp():
	return jsonify({'CPUTemp':rasp.get_cpu_temperature()})

@app.route('/api/getRam')
#@auth.login_required
def getRam():
	return jsonify({'RamInfo':rasp.getRam()})

@app.route('/api/getDiskUsage')
#@auth.login_required
def getDiskUsage():
	return jsonify({'Usage Disk':rasp.getDiskUsage()})

@app.route('/api/getDiskPartition')
#@auth.login_required
def getDiskPartition():
	return jsonify({'Disk Partition':rasp.getDiskPartition()})


@app.route('/api/getBootTime')
#@auth.login_required
def getBootTime():
	return jsonify({'Boot Time':rasp.getBootTime()})

@app.route('/api/getUsers')
#@auth.login_required
def getUsers():
	return jsonify({'Users':rasp.getUsers()})

@app.route('/api/getNetworkInfo')
#@auth.login_required
def getNetworkInfo():
	return jsonify({'Network Info':rasp.getNetworkInfo()})

@app.route('/api/getNetworkConnection')
@auth.login_required
def getNetworkConnection():
	return jsonify({'Network Connection':rasp.getNetworkConnection()})

@app.route('/api/getNetworkInterface')
@auth.login_required
def getNetworkInterface():
	return jsonify({'Network Interface':rasp.getNetworkInterface()})

@app.route('/api/managePid/getAllPid')
@auth.login_required
def getPid():
	return jsonify({'AllPids':rasp.getAllPid()})

@app.route('/api/managePid/kill/<int:pid>', methods=['GET'])
@auth.login_required
def killPid(pid):
	os.kill(pid, signal.SIGTERM)
    	return jsonify({'SIGTERM':pid})

@app.route('/api/getNumberConnectedDevices')
#@auth.login_required
def getLanDevices():
    	#return jsonify({'Number Devices':netw.getNumberConnectedDevices()})
        return str(netw.getNumberConnectedDevices())



if __name__ == '__main__':
        app.run(debug=True,host='0.0.0.0')
